//
//  RegisterViewController.swift
//  ios-chat-app
//
//  Created by Ali Şengür on 5.08.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import FirebaseAuth


class RegisterViewController: UIViewController {

    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signUpButton.layer.cornerRadius = 6
        backButton.layer.cornerRadius = 6
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.cornerRadius = 60
        profileImageView.layer.borderWidth = 2
        profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        
        profileImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTappedProfileImageView))
        profileImageView.addGestureRecognizer(tap)

    }
    
    
    @objc func didTappedProfileImageView() {
        presentActionSheet()
    }
    
    
    
    @IBAction func didTappedSignupButton(_ sender: UIButton) {
        
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty,
            let username = usernameTextField.text, !username.isEmpty,
            password.count > 6 else {
                alertLoginError(message: "Please enter all information to create new account")
                return
        }
        
        
        
        DatabaseManager.shared.userExist(with: email, completion: {[weak self] exists in
            guard let strongSelf = self else { return }
            
            guard !exists else {
                //User already exists
                strongSelf.alertLoginError(message: "User account for that email address already exists")
                return
            }
            
            
            Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
                       guard authResult != nil, error == nil else {
                           debugPrint("Failed to create user")
                           return
                       }
                       
                       let chatUser = User(emailAddress: email, username: username)
                       
                       DatabaseManager.shared.insertUser(with: chatUser){ (success) in
                           if success {
                                // upload image
                               guard let image = strongSelf.profileImageView.image,
                                   let data = image.pngData() else {
                                       return
                               }

                               let fileName = chatUser.profilePictureFileName
                               StorageManager.shared.uploadProfilePicture(with: data, fileName: fileName, completion: { result in
                                   switch result {
                                   case .success(let downloadUrl):
                                       print(downloadUrl)
                                   case .failure(let error):
                                       print("Storage manager error: \(error)")
                                   }
                               })
                           }
                           
                       }
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let mainVC = storyboard.instantiateViewController(identifier: "LoginViewController") as? LoginViewController {
                    self?.present(mainVC, animated: true, completion: nil)
                }
            }
        })

    }
    

    
    @IBAction func didTappedBackButton(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
    }
    

    func alertLoginError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alert.addAction(dismissAction)
        present(alert, animated: true, completion: nil)
    }


}





extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func presentActionSheet() {
        
        let actionSheet = UIAlertController(title: "Profile Picture", message: "How would you like to select a picture?", preferredStyle: .actionSheet)
        
        
        let chooseAction = UIAlertAction(title: "Choose Photo", style: .default) { (action) in
            let vc = UIImagePickerController()
            vc.sourceType = .photoLibrary
            vc.delegate = self
            vc.allowsEditing = true
            self.present(vc, animated: true)
        }
        
        let takeAction = UIAlertAction(title: "Take photo", style: .default) { (action) in
            let vc = UIImagePickerController()
            vc.sourceType = .camera
            vc.delegate = self
            vc.allowsEditing = true
            self.present(vc, animated: true)
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        actionSheet.addAction(chooseAction)
        actionSheet.addAction(takeAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        self.profileImageView.image = selectedImage
    }
    
    
    
}

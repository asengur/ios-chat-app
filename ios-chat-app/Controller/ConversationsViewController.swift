//
//  ConversationsViewController.swift
//  ios-chat-app
//
//  Created by Ali Şengür on 5.08.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import FirebaseAuth




struct Conversation {
    let id: String
    let name: String
    let otherUserEmail: String
    let latestMessage: LatestMessage
}
 
 
 struct LatestMessage {
    let date: String
    let text: String
    let isRead: Bool
 }







class ConversationsViewController: UIViewController {

    
    
    @IBOutlet weak var tableView: UITableView!
    
    private var conversations = [Conversation]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.prefersLargeTitles = true
        setupTableView()
        startListeningForConversations()
    }
    
    
    
    fileprivate func setupTableView() {
        let tableViewCellNib = UINib(nibName: "ConversationTableViewCell", bundle: nil)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(tableViewCellNib, forCellReuseIdentifier: "ConversationCell")
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        validateAuth()
    }
    
    
    

    private func validateAuth() {
        if Auth.auth().currentUser == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewController(identifier: "LoginViewController")
            self.present(loginVC, animated: true, completion: nil)
        }
    }

    
    
    private func startListeningForConversations() {
    
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else {
            return
        }
        
        let safeEmail = DatabaseManager.safeEmail(emailAddress: email)
        
        
        print("current user : \(safeEmail)")
        print("starting conversation fetch..")
        
        DatabaseManager.shared.getAllConversations(for: safeEmail, completion: { [weak self] result in
                   switch result {
                   case .success(let conversations):
                       print("successfully got conversation models")
                       guard !conversations.isEmpty else {
                           return
                       }
                       self?.conversations = conversations
                       DispatchQueue.main.async {
                           self?.tableView.reloadData()
                       }
                   case .failure(let error):
                       print("failed to get convos : \(error)")
                   }
               })
    
    }

    
    
    
    
    @IBAction func didTapComposeButton(_ sender: UIBarButtonItem) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(identifier: "NewConversationViewController") as? NewConversationViewController {
            
            vc.navigationItem.largeTitleDisplayMode = .never
            
            vc.completion = { [weak self] result in
                print("\nResult : \(result)")
                self?.createNewConversation(result: result)
            }
            
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
    
    private func createNewConversation(result: [String: String]) {
        
        guard let name = result["name"], let email = result["email"] else { return }

        let chatVC = ChatViewController(with: email, id: nil)
        chatVC.isNewConversation = true
        chatVC.title = name
        chatVC.navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
}



extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = conversations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell", for: indexPath) as! ConversationTableViewCell
        cell.configure(with: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = conversations[indexPath.row]
        let vc = ChatViewController(with: model.otherUserEmail, id: model.id)
        vc.title = model.name
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // begin delete
            let conversationId = conversations[indexPath.row].id
            tableView.beginUpdates()
            
            DatabaseManager.shared.deleteConversation(conversationId: conversationId, completion: { [weak self] success in
                if success {
                    self?.conversations.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .left)
                }
            })
            
            tableView.endUpdates()
        }
    }
    
}

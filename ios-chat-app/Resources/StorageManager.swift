//
//  StorageManager.swift
//  ios-chat-app
//
//  Created by Ali Şengür on 6.08.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import FirebaseStorage




final class StorageManager {
    
    static let shared = StorageManager()
    private let storage = Storage.storage().reference()
    
    
    public typealias UploadPictureCompletion = (Result<String, Error>) -> Void
    
    
    /// Uploads picture to firebase storage and returns completion with url string to download
    public func uploadProfilePicture(with data: Data,
                                     fileName: String,
                                     completion: @escaping UploadPictureCompletion) {
    
        storage.child("images/\(fileName)").putData(data, metadata: nil){ (metadata, error) in
            guard error == nil else {
                print("Failed to upload data to firebase for picture")
                completion(.failure(StorageErrors.failedToUpload))
                return
            }
            
            self.storage.child("images/\(fileName)").downloadURL{(url, error) in
                guard let url = url else {
                    print("Failed to get download url")
                    completion(.failure(StorageErrors.failedToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print(("Download url returned: \(urlString)"))
                completion(.success(urlString))
            }
        }
    }
    
    
    
    
    
    /// Upload image that will be sent in a conersation message
       public func uploadMessagePhoto(with data: Data,
                                        fileName: String,
                                        completion: @escaping UploadPictureCompletion) {
           storage.child("message_images/\(fileName)").putData(data, metadata: nil){ (metadata, error) in
               guard error == nil else {
                   print("Failed to upload data to firebase for picture")
                   completion(.failure(StorageErrors.failedToUpload))
                   return
               }
               
               self.storage.child("message_images/\(fileName)").downloadURL{(url, error) in
                   guard let url = url else {
                       print("Failed to get download url")
                       completion(.failure(StorageErrors.failedToGetDownloadUrl))
                       return
                   }
                   
                   let urlString = url.absoluteString
                   print(("Download url returned: \(urlString)"))
                   completion(.success(urlString))
               }
           }
       }
    
    
    
    public enum StorageErrors: Error {
        case failedToUpload
        case failedToGetDownloadUrl
    }
    
    
    
    public func downloadURL(for path: String, completion: @escaping (Result< URL, Error>) -> Void) {
        let reference = storage.child(path)
        
        reference.downloadURL(completion: { url, error in
            guard let url = url, error == nil else {
                completion(.failure(StorageErrors.failedToGetDownloadUrl))
                return
            }
            completion(.success(url))
        })
    }
}

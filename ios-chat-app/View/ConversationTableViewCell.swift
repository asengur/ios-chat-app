//
//  ConversationTableViewCell.swift
//  ios-chat-app
//
//  Created by Ali Şengür on 6.08.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import SDWebImage

class ConversationTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var messageTextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageView.layer.cornerRadius = 40
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.borderWidth = 1
        profileImageView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    public func configure(with model: Conversation) {
           self.messageTextLabel.text = model.latestMessage.text
           self.usernameLabel.text = model.name
           
           let path = "images/\(model.otherUserEmail)_profile_picture.png"
           StorageManager.shared.downloadURL(for: path, completion: { result in
               switch result {
               case .success(let url):
                   DispatchQueue.main.async {
                       self.profileImageView.sd_setImage(with: url, completed: nil)
                   }
               case .failure(let error):
                   print("failed to get image url: \(error)")
               }
           })
       }
    
}
